package controller;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import model.Star;
import view.StarFrame;

public class StarManagement {
	StarFrame frame = new StarFrame();
	Star star = new Star();

	public static void main(String[] args) {
		new StarManagement();
	}
	
	class AddInterestListener implements ActionListener
	  {
	     public void actionPerformed(ActionEvent event)
	     {

	    	 String format = frame.getFormat();
	    	 int size = Integer.parseInt((frame.getSize()));
	    	 
	    	 String ans= star.formatStar(size,format);
	    	 frame.ShowStar(ans);
	    	 
	     } 	 
	   
	  }
	
	public StarManagement(){
		ActionListener listener = new AddInterestListener();
		frame.getRunButton().addActionListener(listener);
		
	}
	


}
