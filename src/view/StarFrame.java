package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.Star;

public class StarFrame {
	private JFrame frame;
	private JLabel lbl;
	private JButton runbtn;
	private JButton clearbtn;
	private JTextArea textArea;
	private JLabel resultLabel;
	private JComboBox formatBox;
	private JLabel selectLabel;
	private JTextField typeSize;
	private Star runFormat;
	private Star star;
	
	
	
	public StarFrame(){
		createFrame();	
	}
	
	public void createFrame() {
		
		frame = new JFrame("Star Frame");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 500);
		frame.setLocation(400,50);
		

		//create textArea ------------------------------------
        lbl = new JLabel();
		frame.add(lbl);
		
		textArea = new JTextArea();
		textArea.setBounds(40, 100, 400, 260);
		lbl.add(textArea);
		

		//create SelectFormatBox ------------------------------------
	    formatBox = new JComboBox();
	    formatBox.addItem("1");
	    formatBox.addItem("2");
	    formatBox.addItem("3");
	    formatBox.addItem("4");
	    formatBox.addItem("5");
	    formatBox.setBounds(200, 20, 70, 20);
	    formatBox.setEditable(true);
	    lbl.add(formatBox);
	    

		//create BoxTypeSize ----------------------------------
	    typeSize = new JTextField();
	    typeSize.setBounds(195, 60, 80, 20);
	    lbl.add(typeSize);
		
		
		//create Button --------------------------------------
	    runbtn = new JButton("Run");
	    runbtn.setBounds(120, 380, 70, 40);
	    lbl.add(runbtn); 
	    
	    clearbtn = new JButton("Clear");
	    clearbtn.setBounds(280, 380, 70, 40);
	    lbl.add(clearbtn);
	    
	    frame.setVisible(true);
	    
	    //ActionListener clearbutton -----------------------------
	    
	    clearbtn.addActionListener(new ActionListener() {
	    	 
            public void actionPerformed(ActionEvent e)
            { 
            	textArea.setText("");
                
            }
        });
    
	    	
	}
	
	
	
	public String getFormat(){
		String format1 = (String) formatBox.getSelectedItem();
		return format1;
	}
	
	public String getSize(){
		String size1 = typeSize.getText();
		return size1;
	}
	
	public void ShowStar(String ans){
			textArea.setText(ans);
			
		}
	
	
	
	 public void setListener(ActionListener list) {
			runbtn.addActionListener(list);
		}
	 
	 public JButton getRunButton(){
		 return runbtn;
	 }
	 
	 
	
	
}
